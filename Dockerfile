FROM node:16

ENV LOGIN_APP_URL="http://localhost:3001"
ENV DASHBOARD_APP_URL="http://localhost:3002"
ENV BOOKING_APP_URL="http://localhost:3003"
ENV API_HOST="http://localhost:4000"

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "npm", "start" ]
