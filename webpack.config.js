const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const DefinePlugin = require("webpack/lib/DefinePlugin");
const deps = require("./package.json").dependencies;
const ASSET_PATH = process.env.ASSET_PATH || '/';
const config = require('./config');

module.exports = {
  mode: "development",
  output: {
    publicPath: ASSET_PATH,
  },
  resolve: {
    extensions: [".css", ".scss", ".js", ".jsx"],
  },
  devServer: {
    static: {
      directory: path.join(__dirname, 'public'),
    },
    compress: true,
    port: 3000,
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /bootstrap\.js$/,
        loader: "bundle-loader",

      },
      {
        test: /\.s?css$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              url: {
                filter: (url) => {
                  if (url.startsWith("data:")) {
                    return false;
                  }
                  return true;
                },
              },
            },
          },
          "sass-loader",
        ],
      },
      {
        test: /\.jsx?$/,
        use: ["babel-loader"],
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "public", "index.html"),
    }),
    new DefinePlugin({
      "process.env.API_HOST": JSON.stringify(config.API_HOST)
    }),
    new ModuleFederationPlugin({
      name: "LoginApp",
      remotes: {
        LoginApp: `LoginApp@${config.LOGIN_APP_URL}/remoteEntry.js`,
      },
      shared: {
        react: {
          singleton: true,
          requiredVersion: '*'
        },
        "react-dom": {
          singleton: true,
          requiredVersion: '*'
        },
        'react-router-dom': {
          singleton: true,
          requiredVersion: "*"
        },
      }
    }),
    new ModuleFederationPlugin({
      name: "DashboardApp",
      remotes: {
        DashboardApp: `DashboardApp@${config.DASHBOARD_APP_URL}/remoteEntry.js`,
      },
      shared: {
        react: {
          singleton: true,
          // version: deps.react,
          requiredVersion: '*'
        },
        "react-dom": {
          singleton: true,
          // version: deps["react-dom"],
          requiredVersion: '*'
        }
      }
    }),
    new ModuleFederationPlugin({
      name: "BookingApp",
      remotes: {
        BookingApp: `BookingApp@${config.BOOKING_APP_URL}/remoteEntry.js`,
      },
      shared: {
        react: {
          singleton: true,
          // version: deps.react,
          requiredVersion: '*'
        },
        "react-dom": {
          singleton: true,
          // version: deps["react-dom"],
          requiredVersion: '*'
        }
      }
    }),
  ],
};
