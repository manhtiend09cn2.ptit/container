module.exports = {
  LOGIN_APP_URL: 'http://localhost:3001',
  DASHBOARD_APP_URL: 'http://localhost:3002',
  BOOKING_APP_URL: 'http://localhost:3003',
  API_HOST: process.env.API_HOST
}
