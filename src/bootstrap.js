import React from 'react';
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';

const apiHost = process.env.API_HOST;

const App = React.lazy(() => import("./components/App"));
const Dashboard = React.lazy(() => import("./components/Dashboard"));
const Booking = React.lazy(() => import("./components/Booking"));

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/dashboard",
    element: <Dashboard />,
  },
  {
    path: "/booking",
    element: <Booking />,
  },
]);

const rootElement = document.getElementById("container");
const root = createRoot(rootElement);

root.render(
  <StrictMode>
      <RouterProvider router={router} />
  </StrictMode>
);
