import React, { lazy, Suspense } from "react";

import { createTheme, ThemeProvider } from "@mui/material/styles";
import {
	Grid,
	Container,
	Typography,
	AppBar,
	IconButton,
	Box,
	Avatar,
	Toolbar, Button,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";

import AccountBalanceIcon from "@mui/icons-material/AccountBalance";

import {useParams} from 'react-router-dom';
import Movie from "@mui/icons-material/Movie";


const BookingApp = lazy(() => import("BookingApp/app"));

const defaultTheme = createTheme();

const Booking = () => {

  const { id } = useParams();

	const handleOpenNavMenu = (event) => {
		setAnchorElNav(event.currentTarget);
	};
	const handleOpenUserMenu = (event) => {
		//setAnchorElUser(event.currentTarget);
		localStorage.setItem("access_token", JSON.stringify(''));
		window.location = "/";
	};

	return (
		<ThemeProvider theme={defaultTheme}>
				<AppBar position="static">
					<Container maxWidth="xl">
						<Toolbar disableGutters>
							<Movie
								sx={{ display: { xs: "none", md: "flex" }, mr: 1 }}
							/>
							<Typography
								variant="h6"
								noWrap
								component="a"
								href="/dashboard"
								sx={{
									mr: 2,
									display: { xs: "none", md: "flex" },
									fontFamily: "sans-serif",
									fontWeight: 700,
									letterSpacing: ".1rem",
									color: "inherit",
									textDecoration: "none",
								}}
							>
								Đặt vé xem phim online
							</Typography>

							<Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
								<IconButton
									size="large"
									aria-label="account of current user"
									aria-controls="menu-appbar"
									aria-haspopup="true"
									onClick={handleOpenNavMenu}
									color="inherit"
								>
									<MenuIcon />
								</IconButton>

							</Box>
							<Movie
								sx={{ display: { xs: "flex", md: "none" }, mr: 1 }}
							/>
							<Typography
								variant="h5"
								noWrap
								component="a"
								href=""
								sx={{
									mr: 2,
									display: { xs: "flex", md: "none" },
									flexGrow: 1,
									fontFamily: "monospace",
									fontWeight: 700,
									letterSpacing: ".3rem",
									color: "inherit",
									textDecoration: "none",
								}}
							>
								LOGO
							</Typography>
							<Box
								sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}
							></Box>

							<Box sx={{ flexGrow: 0 }}>

								<IconButton  sx={{ p: 0  , marginRight : 2}}>
									<Avatar alt="" src="" />
								</IconButton>
								<Button
									variant="contained"
									color="error"
									onClick={handleOpenUserMenu}
								>
									Đăng Xuất
								</Button>
							</Box>
						</Toolbar>
					</Container>
				</AppBar>
			<Container maxWidth="xl">
				<Grid p={6}>
					<Suspense fallback={<span>Loading...</span>}>
						<BookingApp />
					</Suspense>
				</Grid>
			</Container>
		</ThemeProvider>
	);
};

export default Booking;
